"""Certificate providers for certget."""

from importlib import import_module


class CertGetProvider:
    """A generic base class for certificate provider implementations."""

    def __init__(self, config_dir, logs_dir, work_dir):
        """Initialize the certificate provider."""
        self.config_dir = config_dir
        self.logs_dir = logs_dir
        self.work_dir = work_dir

    @staticmethod
    def get_provider(provider_name, config_dir, logs_dir, work_dir):
        """Get a concrete certget provider implementation instance."""
        provider_module = import_module(
            ".{}".format(provider_name), __loader__.name
        )
        provider_class = getattr(provider_module, "Provider")
        provider = provider_class(config_dir, logs_dir, work_dir)
        return provider

    def cert(self, email, certificate_name, *domains):
        """Obtain a new certificate."""
        return NotImplemented

    def renew(self):
        """Renew an existing certificate."""
        return NotImplemented
