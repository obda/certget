"""Certificate provider for InterNetworX."""

import os
import shutil
import subprocess
import sys
import tempfile
from contextlib import contextmanager
from itertools import chain

from configobj import ConfigObj

from . import CertGetProvider


class Provider(CertGetProvider):
    """A certificate provider for InterNetworX."""

    def cert(self, email, certificate_name, *domains):
        """Obtain a new certificate."""
        with self._certbot_command() as base_command:
            command = (
                base_command
                + list(
                    chain.from_iterable([("-d", domain) for domain in domains])
                )
                + [
                    "--cert-name",
                    certificate_name,
                    "--text",
                    "--agree-tos",
                    "--email",
                    email,
                    "--expand",
                    "--no-eff-email",
                    "certonly",
                ]
            )
            subprocess.run(command)

    def renew(self):
        """Renew an existing certificate."""
        with self._certbot_command() as base_command:
            command = base_command + ["renew"]
            subprocess.run(command)

    @contextmanager
    def _certbot_command(self):
        inwx_config = {
            "url": "https://api.domrobot.com/xmlrpc/",
            "username": os.environ["CERTGET_INWX_USER"],
            "password": os.environ["CERTGET_INWX_PASS"],
            "shared_secret": "your_shared_secret optional",
        }
        configobj = ConfigObj(encoding="utf-8")
        configobj.update(
            {
                "certbot_dns_inwx:dns_inwx_{}".format(key): value
                for key, value in inwx_config.items()
            }
        )
        with tempfile.NamedTemporaryFile() as fp:
            configobj.write(fp)
            fp.flush()
            yield [
                shutil.which("certbot", path=os.path.dirname(sys.executable)),
                "--non-interactive",
                "--config-dir",
                self.config_dir,
                "--logs-dir",
                self.logs_dir,
                "--work-dir",
                self.work_dir,
                "--authenticator",
                "certbot-dns-inwx:dns-inwx",
                "--certbot-dns-inwx:dns-inwx-credentials",
                fp.name,
                "--certbot-dns-inwx:dns-inwx-propagation-seconds",
                os.environ.get("CERTGET_INWX_PROPAGATION_TIMEOUT", "60"),
            ]
