"""The main ``certget`` command-line interface."""

import click

from .provider import CertGetProvider


main = click.Group()


@main.command()
@click.argument("provider_name", required=True, metavar="<provider>")
@click.argument("domains", nargs=-1, required=True, metavar="<domain>...")
@click.option("--cert-name", default=None)
@click.option("--config-dir", default="/etc/letsencrypt")
@click.option("--logs-dir", default="/var/log/letsencrypt")
@click.option("--work-dir", default="/var/lib/letsencrypt")
@click.option("--email", required=True)
def cert(
    provider_name, domains, cert_name, config_dir, logs_dir, work_dir, email
):
    """Obtain a new Let’s Encrypt certificate."""
    if cert_name is None:
        cert_name = domains[0]
    provider = CertGetProvider.get_provider(
        provider_name, config_dir, logs_dir, work_dir
    )
    provider.cert(email, cert_name, *domains)


@main.command()
@click.argument("provider_name", required=True, metavar="<provider>")
@click.option("--config-dir", default="/etc/letsencrypt")
@click.option("--logs-dir", default="/var/log/letsencrypt")
@click.option("--work-dir", default="/var/lib/letsencrypt")
def renew(provider_name, config_dir, logs_dir, work_dir):
    """Renew certificates."""
    provider = CertGetProvider.get_provider(
        provider_name, config_dir, logs_dir, work_dir
    )
    provider.renew()
