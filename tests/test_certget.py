"""Tests for the ``certget`` package module."""

from certget import __version__


def test_version():
    """Check for the correct version number."""
    assert __version__ == "0.1.0"
